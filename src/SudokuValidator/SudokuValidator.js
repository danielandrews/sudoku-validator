'use strict';

export class SudokuValidator {

    guardMissingValues(board, firstIndex, secondIndex) {
        if (!board[firstIndex] || !board[secondIndex] || !(board[secondIndex].length === 9) || !board[firstIndex][secondIndex] || !board[secondIndex][firstIndex]) {
            throw 'Board is missing entries';
        }
    }

    isBetween1And9(board, firstIndex, secondIndex) {
        return board[firstIndex][secondIndex] > 0 && board[firstIndex][secondIndex] < 10;
    }

    isValid = (board) => {
        
        for (let i = 0; i < 9; i++) {
            let row = new Set();
            let column = new Set();
            let subGrid = new Set();

            for (let j = 0; j < 9; j++) {

                this.guardMissingValues(board, i, j);

                if (!this.isBetween1And9(board, i, j) || !this.isBetween1And9(board, j, i)) {
                    return false;
                }

                if (row.has(board[i][j])) {
                    return false;
                }
                row.add(board[i][j]);

                if (column.has(board[j][i])) {
                    return false;
                }
                column.add(board[j][i]);

                const rowIndex = (3 * Math.floor(i / 3)) + Math.floor(j / 3);
                const colIndex = (3 * (i % 3)) + j % 3;

                if (subGrid.has(board[rowIndex][colIndex])) {
                    return false;
                }

                subGrid.add(board[rowIndex][colIndex]);
            }
        }

        return true;
    };
};