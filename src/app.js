'use strict';

import * as fs from 'fs';
import { SudokuValidator } from './SudokuValidator/SudokuValidator';

if (process.argv.length < 3) {
    console.log('Usage: node ' + process.argv[1] + ' sudoku_input_file_name');
    process.exit(1);
}

const parseBoard = (data) => {
    return data.split('\r\n\r\n').map((row) => {
        return row.split('');
    });
};

const fileName = process.argv[2];

fs.readFile(fileName, 'utf8', (err, data) => {
    if (err) {
        throw err;
    }
    const board = parseBoard(data);
    const sudokuValidator = new SudokuValidator();
    const isSudokuSolutionValid = sudokuValidator.isValid(board);
    console.log("Is sudoku input a valid solution? : " + isSudokuSolutionValid);
    return isSudokuSolutionValid;
});





