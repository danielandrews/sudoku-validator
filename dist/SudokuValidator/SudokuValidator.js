'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SudokuValidator = exports.SudokuValidator = function () {
    function SudokuValidator() {
        var _this = this;

        _classCallCheck(this, SudokuValidator);

        this.isValid = function (board) {

            for (var i = 0; i < 9; i++) {
                var row = new Set();
                var column = new Set();
                var subGrid = new Set();

                for (var j = 0; j < 9; j++) {

                    _this.guardMissingValues(board, i, j);

                    if (!_this.isBetween1And9(board, i, j) || !_this.isBetween1And9(board, j, i)) {
                        return false;
                    }

                    if (row.has(board[i][j])) {
                        return false;
                    }
                    row.add(board[i][j]);

                    if (column.has(board[j][i])) {
                        return false;
                    }
                    column.add(board[j][i]);

                    var rowIndex = 3 * Math.floor(i / 3) + Math.floor(j / 3);
                    var colIndex = 3 * (i % 3) + j % 3;

                    if (subGrid.has(board[rowIndex][colIndex])) {
                        return false;
                    }

                    subGrid.add(board[rowIndex][colIndex]);
                }
            }

            return true;
        };
    }

    _createClass(SudokuValidator, [{
        key: 'guardMissingValues',
        value: function guardMissingValues(board, firstIndex, secondIndex) {
            if (!board[firstIndex] || !board[secondIndex] || !(board[secondIndex].length === 9) || !board[firstIndex][secondIndex] || !board[secondIndex][firstIndex]) {
                throw 'Board is missing entries';
            }
        }
    }, {
        key: 'isBetween1And9',
        value: function isBetween1And9(board, firstIndex, secondIndex) {
            return board[firstIndex][secondIndex] > 0 && board[firstIndex][secondIndex] < 10;
        }
    }]);

    return SudokuValidator;
}();

;