'use strict';

var _fs = require('fs');

var fs = _interopRequireWildcard(_fs);

var _SudokuValidator = require('./SudokuValidator/SudokuValidator');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

if (process.argv.length < 3) {
    console.log('Usage: node ' + process.argv[1] + ' sudoku_input_file_name');
    process.exit(1);
}

var parseBoard = function parseBoard(data) {
    return data.split('\r\n\r\n').map(function (row) {
        return row.split('');
    });
};

var fileName = process.argv[2];

fs.readFile(fileName, 'utf8', function (err, data) {
    if (err) {
        throw err;
    }
    var board = parseBoard(data);
    var sudokuValidator = new _SudokuValidator.SudokuValidator();
    var isSudokuSolutionValid = sudokuValidator.isValid(board);
    console.log("Is sudoku input a valid solution? : " + isSudokuSolutionValid);
    return isSudokuSolutionValid;
});